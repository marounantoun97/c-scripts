//
//  sort.cpp
//  Algo1
//
//  Created by Dany Mezher on 10/9/18.
//  Copyright © 2018 Dany Mezher. All rights reserved.
//

#include <iostream>
#include <sys/time.h>
using namespace std;

void Bubble(float *A, int n){
    for(int i=1;i<n;++i){
        int nbPerm=0;
        for(int j=0;j<n-i;++j){
            if (A[j]>A[j+1]){
                float tmp=A[j];
                A[j]=A[j+1];
                A[j+1]=tmp;
                ++nbPerm;
            }
        }
        if (nbPerm==0)
            return;
    }
}

void QSort(float *A, int n){
    if (n>1){
        int i=1,j=n-1;
        float p=A[0];
        while(i<=j){
            while(A[i]<p && i<=j){
                ++i;
            }
            while(A[j]>=p && i<=j){
                --j;
            }
            if (i<j){
                float t=A[i];
                A[i]=A[j];
                A[j]=t;
            }
        }
        A[0]=A[j];
        A[j]=p;
        QSort(A,j);
        QSort(A+i,n-i);
    }
}

struct node{
    float v;
    node *next;
};

void diviser(node *tete, node **Gigi, node **Bella){
    if (tete!=0){
        node *p=tete->next;
        tete->next=*Gigi;
        *Gigi=tete;
        tete=p;
        diviser(tete,Bella,Gigi);
    }
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
node* fusion(node *L1, node *L2){
    if (L1==0){
        return L2;
    }
    if (L2==0){
        return L1;
    }
    if (L1->v<L2->v){
        L1->next = fusion(L1->next, L2);
        return L1;
    }
    else{
        L2->next = fusion(L1, L2->next);
        return L2;
    }
 
}

node* trier(node *tete){
    if(tete==0 || tete->next==0){
        return tete;
    }
    node *L1=0,*L2=0;
    diviser(tete,&L1,&L2);
    L1=trier(L1);
    L2=trier(L2);
    return fusion(L1,L2);
}

int main(){
    int const N=2000;
    float *A=new float[N],*B=new float[N];
    node *tete=0;
    for(int i=0;i<N;++i){
        A[i]=B[i]=rand() % 100;
        node *p=tete;
        tete=new node;
        tete->next=p;
        tete->v=A[i];
    }
    timeval t1,t2,t3,t4;
    gettimeofday(&t1,0);
    QSort(B,N);
    gettimeofday(&t2,0);
    cout<<"Quick="<<t2.tv_sec-t1.tv_sec+1e-6*(t2.tv_usec-t1.tv_usec)<<endl;
    Bubble(A,N);
    gettimeofday(&t3,0);
    cout<<"Bubble="<<t3.tv_sec-t2.tv_sec+1e-6*(t3.tv_usec-t2.tv_usec)<<endl;
    tete=trier(tete);
    gettimeofday(&t4, 0);
    cout<<"Fusion="<<t4.tv_sec-t3.tv_sec+1e-6*(t4.tv_usec-t3.tv_usec)<<endl;
}


void quickSort(int *A , int n){
    if(n>1){
        int pivot = A[0];
        int i = 1;
        int j = n-1
        while(i<=j){
            while(A[i]<pivot && i<=j){
                ++i;
            }
            while(A[j]>pivot && i<=j){
                --j;
            }
            if(i<j){
                swap(A[i], A[j]);
            }

        }
        swap(pivot, A[j]);
        quickSort(A,j);
        quickSort(A+i, n-i);
    }
}


void diviser(node * tete, node ** Gigi, node ** Bella){
    node * p = tete->next;
    tete->next = *Gigi;
    *Gigi = tete;
    tete = p;
    diviser(tete, Bella, Gigi);
}

node * fusion (node * l1, node * l2){
    if(l1 ==0){
        return(l2);
    }
    else if(l2 ==0){
        return (l1);
    }

    else if (l1->v < l2->v){
        l1->next = fusion(l1->next, l2);
        return l1;
    }
    else{
        l2->next = fusion(lk1, l2->next);
        return(l2);
    }
}


node * trier(node * tete){
    if(tete==0 || tete->next == 0)
    {
        return tete;
    }
    node * l1=0;
    node * l2=0;
    diviser(tete, &l1, &l2);
    l1 = trier(l1);
    l2 = trier(l2);
    return(fusion(l1,l2));


}
