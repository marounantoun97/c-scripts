#include <iostream>

using namespace std;
int i=1;

void hanoii(int n, char from, char to, char us){
  if (n==1){
     cout<<i++<<" from "<<from<<" to "<<to<<endl;
  }
  else{
  hanoii(n-1,from,us,to);
  hanoii(1,from,to, us);
  hanoii(n-1,us,to,from);
}
}

struct noeud{
  int v;
  noeud *n;
};

int compter(noeud *tete){
  if (tete==0){
    return 0;
  }
  return 1+compter(tete->n);
}

int somme(noeud *tete){
  if (tete==0){
    return 0;
  }
  return tete->v+somme(tete->n);
}

void affiche(noeud *tete){
  if (tete==0){
    return;
  }
  cout<<tete->v<<" ";
  affiche(tete->n);
  cout<<tete->v<<" ";
}

bool appartient(int x, noeud *tete){
  if (tete==0){
    return false;
  }
  if (tete->v==x){
    return true;
  }
  return appartient(x,tete->n);
}

int max(noeud *tete){
   if (tete==0){
    throw "Error";
   }
   if (tete->n==0){ // Condition d'arret
     return tete->v;
   }
   int M=max(tete->n);
   if (tete->v>M){
    return tete->v;
   }
   else{
     return M;
   }
}

void affiche(char E[8][8]){
 cout<<"========"<<endl;
 for(int i=0;i<8;++i){
   for(int j=0;j<8;++j){
     cout<<E[i][j];
   }
   cout<<endl;
 }
}


//Problème des reines sur l'échiquier.

bool check(char E[8][8], int r, int c){
  for(int i=r+1;i<8;++i){
   if (E[i][c]=='X')
     return false;
   if (c+i-r<8 && E[i][c+i-r]=='X'){
     return false;
   }
   if (c-i+r>=0 && E[i][c-i+r]=='X'){
     return false;
   }
 }
 return true;
}

void placer(int n, char E[8][8]){
  if (n==0){
    affiche(E);
  } 
  for(int c=0;c<8;++c){
    E[n-1][c]='X';
    if (check(E,n-1,c)){
      placer(n-1,E);
    }
    E[n-1][c]='.';
  }
}

struct Board{
   char E[8][8];
};

#include <list>

bool check1(Board b, int r, int c){
  for(int i=0;i<r;++i){
    if (b.E[i][c]=='X') return false;
    if (c+r-i<8 && b.E[i][c+r-i]=='X') return false;
    if (c-r+i>=0 && b.E[i][c-r+i]=='X') return false;
  }
  return true;
}


list<Board>placer1(int n){
  list<Board> ret;
  if (n==1){
     for(int i=0;i<8;++i){
       Board b;
       for(int r=0;r<8;++r){
         for(int c=0;c<8;++c){
           b.E[r][c]='.';
         }
       }
       b.E[0][i]='X';
       ret.push_back(b);
     }
     return ret;
  }
  else{
     list<Board> t=placer1(n-1);
     for(Board b : t){
       for(int c=0;c<8;++c){
         b.E[n-1][c]='X';
         if (c>0){
           b.E[n-1][c-1]='.';
         }
         if (check1(b,n-1,c))
            ret.push_back(b);
       }
     }
     return ret;
  }
}


bool placer2(char E[8][8], int n){
  if (n==0){
   affiche(E);
   return true;
  }
  else{
    for(int c=0;c<8;++c){
      E[n-1][c]='X';
      if (check(E,n-1,c)==true){
        if (placer2(E,n-1)){
          return true;
        }
      }
      E[n-1][c]='.'; 
    }
    return false;
  } 
}

int main(){
  noeud *tete=0;
  for(int i=20;i>=0;--i){
    noeud *p=tete;
    tete=new noeud;
    tete->n=p;
    tete->v=i;
  }
  affiche(tete);
  char E[8][8];
  for(int i=0;i<8;++i){
    for(int j=0;j<8;++j){
      E[i][j]='.';
    }
  }
 // cout<<"Recursivite 1"<<endl;
 // placer2(E,8);
 // return 1;
 // cout<<"Recursivite 2"<<endl;
  list<Board>l=placer1(8);
  for(Board b : l){
   affiche(b.E);
  }
  return 0;
}