// Maroun Antoun - 160180 

#include <map>
#include <algorithm>
#include <iostream>
#include <vector>



using namespace std;

// Exercice 1 - PGCD

int pgcd(int a, int b){
    if(b != 0){return pgcd(b, a%b);}
    else{return a;}
}


//Exercice 2 - Calendrier 

struct event{
    int from;
    int to;
    float x;
    float width;
};

struct arbre
{
    event *i;
    arbre *fg, *fd;
    int maximum;
};

arbre * newNode(event& e)
{
    arbre *tmp   = new arbre;
    tmp->i       = &e;
    tmp->maximum = e.to;
    tmp->fg      = 0;
    tmp->fd      = 0;
    return tmp;
};

arbre *putArbre(arbre *root, event& e)
{
    e.x = 0;
    e.width = 1;
    if (root == 0)
        return newNode(e);
    
    int firstFrom = root->i->from;
    
    if (e.from < firstFrom){
        root->fg = putArbre(root->fg, e);
    }
    else{
        root->fd = putArbre(root->fd, e);
    }
    if (root->maximum < e.to)
        root->maximum = e.to;
    
    return root;
}


bool check (event& e1, event& e2)
{
    if (e1.from < e2.to && e2.from < e1.to){
        return true;
        }

    else{
        return false;
    }
}

event * collision (arbre *root, event& i)
{
    if (root == 0) {
        return 0;
        }
    
    if (check(*(root->i), i)){
        return root->i;
        }
    
    if (root->fg != 0 && root->fg->maximum >= i.from){
        return collision(root->fg, i);
        }
    
    return collision(root->fd, i);
}

void putEvents(vector<vector<event*>> &overlappedEvents){
    size_t size = overlappedEvents.size();
    
    map<event*,vector<int>> eventsMap;
    map<event*,vector<int>>::iterator it;
    vector<vector<event*>> groupOfOverlappedEvents;
    vector<event*> group;
    
    
    for(int i = 0 ; i < size ; ++i){
        for(int j = 0 ; j < 2 ; ++j){
            eventsMap[overlappedEvents[i][j]].push_back(i);
        }
    }
    
    for(it = eventsMap.begin(); it != eventsMap.end() ; ++it){
        group.clear();
        group.push_back(it->first);
        for(int i = 0 ; i < it->second.size() ; ++i){
            for(int j = 0 ; j < 2 ; ++j){
                if(!(find(group.begin(), group.end(), overlappedEvents[it->second[i]][j]) != group.end())){
                    group.push_back(overlappedEvents[it->second[i]][j]);
                    for(int k = 0 ; k < eventsMap[overlappedEvents[it->second[i]][j]].size() ; ++k){
                        for(int u = 0 ; u < 2 ; ++u){
                            if(!(find(group.begin(), group.end(), overlappedEvents[eventsMap[overlappedEvents[it->second[i]][j]][k]][u]) != group.end())){
                                group.push_back(overlappedEvents[eventsMap[overlappedEvents[it->second[i]][j]][k]][u]);
                            }
                        }
                    }
                }
            }
        }
        groupOfOverlappedEvents.push_back(group);
    }
    
    
    for(int i = 0 ; i < groupOfOverlappedEvents.size() ; ++i){
        group = groupOfOverlappedEvents[i];
        if(group.size() > 3){
            bool b = false;
            for(int i = 0 ; i < group.size() ; ++i){
                if((group[i]->width >= 0.33 && group[i]->width <= 0.34) || group[i]->width == 0.5){
                    b = true;
                }
            }
            if(b){
                break;
            }
            sort(group.begin(), group.end(), [](const event* e1, const event* e2)
                 {
                     return e1->from < e2->from;
                 });
            
            int k = 3;
            group[0]->x = 0;
            group[0]->width = 0.33;
            
            group[1]->x = 0.33;
            group[1]->width = 0.33;
            
            group[2]->x = 0.66;
            group[2]->width = 0.33;
            
            for(int j = 3 ; j < group.size(); ++j){
                for(int u = 0 ; u < k ; ++u){
                    if(group[u]->to <= group[j]->from){
                        group[j]->x = group[u]->x;
                        group[j]->width = group[u]->width;
                    }
                }
                
                ++k;
            }
            
            
        }else if(group.size() == 3){
            group[0]->x = 0;
            group[0]->width = 0.33;
            
            group[1]->x = 0.33;
            group[1]->width = 0.33;
            
            group[2]->x = 0.66;
            group[2]->width = 0.33;
        }else if(group.size() == 2){
            group[0]->x = 0;
            group[0]->width = 0.5;
            
            group[1]->x = 0.5;
            group[1]->width = 0.5;
        }else if (group.size() == 1){
            group[0]->x = 0;
            group[0]->width = 1;
        }
    }
    
    
}


void layout(event* e, int n)
{
    arbre *root = 0;
    root = putArbre(root, e[0]);
    vector<vector<event*>> overlappedEvents;
    
    for (int i=1; i < n; ++i)
    {
        event * fin = collision(root, e[i]);
        if (fin != 0){
            vector<event*> e1;
            e1.push_back(&e[i]);
            e1.push_back(fin);
            overlappedEvents.push_back(e1);
        }
        root = putArbre(root, e[i]);
    }
    
    putEvents(overlappedEvents);
}




// void information(event *calendrier, int n)
// {
//     cout << "Event\tfrom\tto\tpos\twidth" << endl;
//     for (int i = 0; i < n; ++i)
//     {
//         cout << i << "\t" << calendrier[i].from << "\t" << calendrier[i].to << "\t" << calendrier[i].x << "\t" << calendrier[i].width << endl;
//     }
//     cout << endl;
// }

// int main()
// {
//     int n = 5;
//     event *calendrier;
//     calendrier = new event[n];
    
//     calendrier[0] = event{ 8,14 };
//     calendrier[1] = event{ 9,11 };
//     calendrier[2] = event{ 10,16 };
//     calendrier[3] = event{ 12,17 };
//     calendrier[4] = event{ 15,18 };
//     // calendrier[0] = event{ 0,1 };
//     // calendrier[1] = event{ 1,3 };
//     // calendrier[2] = event{ 2,6 };
//     // calendrier[3] = event{ 3,6 };
//     // calendrier[4] = event{ 9,10 };
//     // calendrier[5] = event{ 10,12 };
//     // calendrier[6] = event{ 11,12 };
//     // calendrier[7] = event{ 12,15 };
//     // calendrier[8] = event{ 13,15 };
//     // calendrier[9] = event{ 14,15 };
    
//     layout(calendrier, n);
//     information(calendrier, n);
    
//     return 0;
// }





