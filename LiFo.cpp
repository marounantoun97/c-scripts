// Last In First Out ou pile

#include <iostream>
#include <string.h>

using namespace std;

class LiFo
{
  public:
    virtual void push(float value) = 0;
    // throws Exception (" add error ") if adding fails
    virtual void pop() = 0;
    // throws Exception (" pop error ") if popping from empty
    //list
    virtual float top() = 0;
    // throws Exception (" empty list ") if popping from empty
    //list
    virtual bool isEmpty() = 0;
    virtual ~LiFo() {}
};

class ArrayLiFo : public LiFo
{
    int nbOfElements;
    int maxSize;
    float *values;

  public:
    ArrayLiFo(int maxSize = 10)
    {
        this->maxSize = maxSize; 
        this->nbOfElements = 0;
        values = new float[maxSize];
    }
    void push(float value){
        if(nbOfElements == maxSize){
            throw "Error...";
        }

        values[nbOfElements] = value;
        ++nbOfElements;

    }
    void pop(){
        if(isEmpty() == true){
            throw "Error...";
        }
        --nbOfElements;
    }
    float top(){
        if(isEmpty() == true){
            throw "Error...";
        }
        nbOfElements--;
        return values[nbOfElements];
        
    }
    bool isEmpty()
    {
        if (nbOfElements == 0)
        {
            return true;
        }
        return false;
    }
    ~ArrayLiFo(){
        delete [] values; 
    }
};

class LinkedLiFo : public LiFo
{
    struct node{
        node * next;
        float value;
    };

    int nbOfElements;
    node * tete;

  public:
    LinkedLiFo(){
        this->nbOfElements = 0;
        this->tete = 0;
    }
    void push(float value){
        node * t = new node();
        t->value = value;
        t->next = tete;
        tete = t;
        ++nbOfElements;
    }

    void pop(){
        if(isEmpty() == true){
            throw "Error...";
        }
        node *t = new node();
        t->next = tete;
        tete = tete->next;
        delete t; 
        --nbOfElements;
    }

    float top(){
        if(isEmpty() == true){
            throw "Error...";
        }
        node *t = new node();
        t->next = tete;
        float t_val = tete->value; 
        tete = tete->next;
        --nbOfElements;
        delete t;
        return t_val;
    }

    bool isEmpty(){
        if(nbOfElements == 0){
            return true;
        }
        return false;
    }
    ~LinkedLiFo(){
        while(isEmpty() == false){
            this->pop();
        }
    }

    // void reverse(){
    //     if(isEmpty()){
    //         throw "Error...";
    //     }
    //     node * t = new node();

    //     while(tete->next != 0){
    //     t->next = this->top();
    //     }
    //     tete = t;
    // }
};


int main(){
    cout<<"Creating the ArrayLiFo: "<<endl;
    LiFo * lifo = new ArrayLiFo(10);
    cout<<"push.."<<endl;
    for(int i=10; i>0; --i){
        cout<<"Pushing: "<<i<<endl;
        lifo->push(i);
    }
    cout<<"top.."<<endl;
    for(int i=0; i<10; ++i){
        cout<<lifo->top()<<endl;
    }
    cout<<"######################################################"<<endl;

    cout<<"Creating the Linked Lifo: "<<endl;
    LiFo * li_lifo = new LinkedLiFo();
    cout<<"push.."<<endl;
    for(int i=10; i>0; --i){
        cout<<"Pushing: "<<i<<endl;
        li_lifo->push(i);
    }
    // cout<<"top.."<<endl;
    // for(int i=0; i<10; ++i){
    //     cout<<li_lifo->top()<<endl;
    // }
    cout<<"############################"<<endl;
    cout<<"Creating the reversed list: "<<endl;

    LiFo * reversedList = new LinkedLiFo();

    cout<<"push..."<<endl;
    for(int i=0; i<10; ++i){
        float top = li_lifo->top();
        cout<<"pushing: "<<top<<endl;
        reversedList->push(top);
    }
    cout<<"top of reversed list"<<endl;
    for(int i=0; i<10; ++i){
        cout<<reversedList->top()<<endl;
    }

}
