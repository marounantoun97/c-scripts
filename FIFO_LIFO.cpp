//
//  FIFO_LIFO.cpp
//  TPC_1_FIFO_LIFO
//
//  Created by Alain khoury on 9/27/18.
//  Copyright © 2018 Alain khoury. All rights reserved.
//

#include <stdio.h>
#include "string"
#include <cassert>
#include "iostream"

using namespace std;

class Exception {
    std :: string _m ;
    public :
    Exception ( std :: string message ) : _m ( message ) {}
    std :: string message () const { return _m ;}
    virtual ~ Exception () {}
};


// Last In First Out ou pile
class LiFo {
public :
    virtual void push ( float value ) =0;
    // throws Exception (" add error ") if adding fails
    virtual void pop () =0;
    // throws Exception (" pop error ") if popping from empty list
    virtual float top () =0;
    // throws Exception (" empty list ") if popping from empty list
    virtual bool isEmpty () =0;
    virtual ~ LiFo () {}
};

// First In , First Out ou File
class FiFo {
public :
    virtual void push ( float value ) =0;
    // throws Exception (" add error ") if adding fails
    virtual void pop () =0;
    // throws Exception (" pop error ") if popping from empty list
    virtual float top () =0;
    // throws Exception (" empty list ") if popping from empty list
    virtual bool isEmpty () =0;
    virtual ~ FiFo () {}
};


class ArrayFiFo : public FiFo {
    // implement a FIFO using array of floats
private:
    float* _queue;
    int _size;
    
    // an element must always be added from tail side and removed from head side
    
    int _head;
    int _tail;
    
public :
    ArrayFiFo();
    ArrayFiFo ( int maxSize ) ;
    void push ( float value ) ;
    void pop () ;
    float top () ;
    bool isEmpty () ;
    ~ ArrayFiFo () ;
};


class LinkedFiFo : public FiFo {
    // implement a FIFO using a linked list
private:
    
    struct Node{
        float data;
        Node* next;
    };
    
    Node* _head;
    Node* _tail;
    
public :
    LinkedFiFo () ;
    void push ( float value ) ;
    void pop () ;
    float top () ;
    bool isEmpty () ;
    ~LinkedFiFo() ;
};

class ArrayLiFo : public LiFo {
    // implement a LIFO using array of floats
private:
    float* _stack;
    int _size;
    int _capactiy;
    
public :
    ArrayLiFo();
    ArrayLiFo ( int maxSize ) ;
    void push ( float value ) ;
    void pop () ;
    float top () ;
    bool isEmpty () ;
    ~ ArrayLiFo () ;
};
class LinkedLiFo : public LiFo {
    // implement a LIFO using a linked list
private:
    struct Node{
        float data;
        Node* next;
    };
    
    Node* _head;
    
public :
    LinkedLiFo () ;
    void push ( float value ) ;
    void pop () ;
    float top () ;
    bool isEmpty () ;
    ~ LinkedLiFo () ;
};

// MARK: ARRAYFIFO

ArrayFiFo::ArrayFiFo(){
    _size = 5;
    _queue = new float[_size];
    
    _head = -1;
    _tail = -1;
}

ArrayFiFo::ArrayFiFo(int maxSize){
    _size = maxSize;
    _queue = new float[_size];
    
    _head = -1;
    _tail = -1;
    
}

bool ArrayFiFo::isEmpty(){
    if(_head == -1 && _tail == -1){
        return true;
    }
    return false;
}

float ArrayFiFo::top(){
    if(isEmpty()){
        throw Exception("empty list");
    }
    return _queue[_head];
    
}

void ArrayFiFo::push(float value){
    if(((_tail + 1)%_size) == _head){
        // if the queue is full then throw exception
        throw Exception("add error");
//
//        ArrayFiFo* temp = new ArrayFiFo(_size*2);
//
//        while (!isEmpty()) {
//            temp->push(this->top());
//            this->pop();
//        }
//
//        delete[] _queue;
//
//        this->_queue = temp->_queue;
//        this->_head = temp->_head;
//        this->_tail = temp->_tail;
//        this->_size = temp->_size;
//
//        _tail = (_tail + 1) % _size;
        
    }else if (isEmpty()){
        _head = 0;
        _tail = 0;
        
    }else {
        _tail = (_tail + 1) % _size;
    }
    
    _queue[_tail] = value;
}

void ArrayFiFo::pop(){
    if(isEmpty()){
        throw Exception("pop error");
    }else if(_head == _tail){
        _head = -1;
        _tail = -1;
        
    }
    else{
        _head = (_head + 1) % _size;
    }
}

ArrayFiFo::~ArrayFiFo(){
    delete[] _queue;
}

// MARK: LINKEDFIFO

// adding element to tail and removing from head

LinkedFiFo::LinkedFiFo(){
    _head = 0;
    _tail = 0;
}

void LinkedFiFo::pop(){
    if(isEmpty()){
        throw Exception("pop error");
    }
    Node* temp = _head;
    if(_head == _tail){
        _head = 0;
        _tail = 0;
    }else{
        _head = _head->next;
    }
    
    delete temp;
}


bool LinkedFiFo::isEmpty(){
    if(_head == 0 && _tail == 0){
        return true;
    }
    
    return false;
}

float LinkedFiFo::top(){
    if(_head == 0){
        throw Exception("empty list");
    }
    
    return _head->data;
}

void LinkedFiFo::push(float value){
    Node* newElement = new Node;
    newElement->data = value;
    newElement->next = 0;
    
    if(_tail == 0 && _head == 0){
        _tail = newElement;
        _head = newElement;
    }else{
        _tail->next = newElement;
        
        _tail = newElement;
    }
}

LinkedFiFo::~LinkedFiFo(){
    while(!isEmpty()){
        this->pop();
    }
}

// MARK: ARRAYLIFO

ArrayLiFo::ArrayLiFo(){
    
    _size = 0;
    _capactiy = 5;
    _stack = new float[_capactiy];
}

ArrayLiFo::ArrayLiFo(int maxSize){
    
    _size = 0;
    _capactiy = maxSize;
    _stack = new float[_capactiy];
    
}

ArrayLiFo::~ArrayLiFo(){
    delete [] _stack;
}


bool ArrayLiFo::isEmpty(){
    return _size == 0;
}

void ArrayLiFo::push(float value){
    if(_size == _capactiy){
        
        throw Exception("add error");
        
//        float* temp = _stack;
//        _capactiy *= 2;
//        _stack = new float[_capactiy];
//
//        memcpy(_stack, temp, _size*sizeof(float));
//
//        delete[] temp;
        
    }
    
    _stack[_size] = value;
    
    _size++;
}

float ArrayLiFo::top(){
    if(_size == 0){
        throw Exception("empty list");
    }
    
    return _stack[_size - 1];
}

void ArrayLiFo::pop(){
    if(_size == 0){
        throw Exception("pop error");
    }
    
    _size -= 1;
}


// MARK: LINKEDLIFO

LinkedLiFo::LinkedLiFo(){
    _head = 0;
}

bool LinkedLiFo::isEmpty(){
    return _head == 0;
}

void LinkedLiFo::push(float value){
    Node* newElement = new Node;
    newElement->data = value;
    newElement->next = _head;
    _head = newElement;
}

void LinkedLiFo::pop(){
    if(_head == 0){
        throw Exception("pop error");
    }
    
    Node* elementToBeDeleted = _head;
    _head = _head->next;
    
    delete elementToBeDeleted;
}

float LinkedLiFo::top(){
    if(_head == 0){
        throw Exception("empty list");
    }
    
    return _head->data;
}

LinkedLiFo::~LinkedLiFo(){
    while (_head != 0) {
        pop();
    }
}


// MARK : Test FiFo

template <typename T>
class TEST_FIFO{
    
private:
    T* queue;
    
public:
    TEST_FIFO(){
        queue = new T();
    }
    
    int startArrayTest(){
        
        queue->push(0);
        queue->push(1);
        queue->push(2);
        queue->push(3);
        queue->push(4);
        
        assert(queue->top() == 0);
        queue->pop();
        
        queue->push(5);
        try{
            queue->push(6);
        }catch(Exception e){
            cout<<"Array is full --> add error\n";
            cout << e.message()<<endl;
        }
        
        assert(queue->isEmpty() == false);
        
        assert(queue->top() == 1);
        queue->pop();
        
        assert(queue->isEmpty() == false);
        
        assert(queue->top() == 2);
        queue->pop();
        assert(queue->top() == 3);
        queue->pop();
        assert(queue->top() == 4);
        queue->pop();
        
        assert(queue->isEmpty() == false);
        
        
        assert(queue->top() == 5);
        queue->pop();
        queue->push(7);
        assert(queue->top() == 7);
        queue->pop();
        
        assert(queue->isEmpty() == true);
        
        try{
            queue->pop();
            return 1;
        }catch(Exception e){
            cout<<"Array is empty --> pop error\n";
            cout << e.message()<<endl;
        }
        
        return 0;
    }
    
    int startLinkedListTest(){
        
        queue->push(0);
        queue->push(1);
        queue->push(2);
        queue->push(3);
        queue->push(4);
        
        assert(queue->top() == 0);
        queue->pop();
        
        queue->push(5);
        try{
            queue->push(6);
        }catch(Exception e){
            cout << e.message()<<endl;
            return 1;
        }
        
        assert(queue->isEmpty() == false);
        
        assert(queue->top() == 1);
        queue->pop();
        queue->push(7);
        assert(queue->top() == 2);
        queue->pop();
        assert(queue->top() == 3);
        queue->pop();
        assert(queue->top() == 4);
        queue->pop();
        assert(queue->top() == 5);
        queue->pop();
        assert(queue->top() == 6);
        queue->pop();
        
        
        assert(queue->isEmpty() == false);
        
        assert(queue->top() == 7);
        queue->pop();
        
        assert(queue->isEmpty() == true);

        
        
        try{
            queue->pop();
            return 1;
        }catch(Exception e){
            cout<<"LinkedList is empty --> pop error\n";
            cout << e.message()<<endl;
        }
        
        return 0;
    }
    
};

// MARK : Test LiFo


template <typename T>
class TEST_LIFO{
    
private:
    T* _stack;
    
public:
    TEST_LIFO(){
        _stack = new T();
    }
    
    int startArrayTest(){
        
        _stack->push(0);
        _stack->push(1);
        _stack->push(2);
        _stack->push(3);
        _stack->push(4);
        
        assert(_stack->top() == 4);
        _stack->pop();
        
        _stack->push(5);
        try{
            _stack->push(6);
            return 1;
        }catch(Exception e){
            cout<<"Array is full --> add error\n";
            cout << e.message()<<endl;
        }
        
        assert(_stack->isEmpty() == false);
        
        assert(_stack->top() == 5);
        _stack->pop();
        assert(_stack->top() == 3);
        _stack->pop();
        
        assert(_stack->isEmpty() == false);
        
        
        assert(_stack->top() == 2);
        _stack->pop();
        assert(_stack->top() == 1);
        _stack->pop();
        
        
        assert(_stack->isEmpty() == false);
        
        assert(_stack->top() == 0);
        _stack->pop();
        
        assert(_stack->isEmpty() == true);
        
        try{
            _stack->pop();
            return 1;
        }catch(Exception e){
            cout<<"Array is empty --> pop error\n";
            cout << e.message()<<endl;
        }
        
        return 0;
    }
    
    int startLinkedListTest(){
        
        _stack->push(0);
        _stack->push(1);
        _stack->push(2);
        _stack->push(3);
        _stack->push(4);
        
        assert(_stack->top() == 4);
        _stack->pop();
        
        _stack->push(5);
        try{
            _stack->push(6);
        }catch(Exception e){
            cout << e.message()<<endl;
            return 1;
        }
        
        assert(_stack->isEmpty() == false);
        
        assert(_stack->top() == 6);
        _stack->pop();
        _stack->push(7);
        assert(_stack->top() == 7);
        _stack->pop();
        assert(_stack->top() == 5);
        _stack->pop();
        assert(_stack->top() == 3);
        _stack->pop();
        assert(_stack->top() == 2);
        _stack->pop();
        assert(_stack->top() == 1);
        _stack->pop();
        
        
        assert(_stack->isEmpty() == false);
        
        assert(_stack->top() == 0);
        _stack->pop();
        
        assert(_stack->isEmpty() == true);
        
        
        
        try{
            _stack->pop();
            return 1;
        }catch(Exception e){
            cout<<"LinkedList is empty --> pop error\n";
            cout << e.message()<<endl;
        }
        
        return 0;
    }
    
};

int main() {
    
    TEST_FIFO<ArrayFiFo> testArrayFIFO = TEST_FIFO<ArrayFiFo>();
    TEST_FIFO<LinkedFiFo> testLinkedListFIFO = TEST_FIFO<LinkedFiFo>();

    assert(testArrayFIFO.startArrayTest() == 0);
    assert(testLinkedListFIFO.startLinkedListTest() == 0);
    
    TEST_LIFO<ArrayLiFo> testArrayLIFO = TEST_LIFO<ArrayLiFo>();
    TEST_LIFO<LinkedLiFo> testLinkedListLIFO = TEST_LIFO<LinkedLiFo>();
    
    assert(testArrayLIFO.startArrayTest() == 0);
    assert(testLinkedListLIFO.startLinkedListTest() == 0);
    
    return 0;
}

