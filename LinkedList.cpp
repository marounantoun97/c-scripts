#include <iostream>



using namespace std;

#include <string.h>


class Exception {
	string _m;
public :
	Exception (string message ):_m( message ){}
	string message () const { return _m ;}
	virtual ~ Exception () {}
};

class MyList{
  
public:
    virtual int   count()=0;                       // abstract int count();
    virtual float get(int i)=0;                  // i=0 for first element...
    virtual void  insert(float x, int position)=0;
    virtual void  put(float x, int position)=0;
    virtual void  remove(int position)=0;
    virtual       ~MyList(){}
};



class LinkedList: public MyList{
	struct node {
		float   valeurContenue;
		node  * nextNode;
	};

	int    currentEltCount;
	node * tete;
	
public:
	LinkedList(){
		tete            = 0;
		currentEltCount = 0;
	}
	
	~ LinkedList() {
		while(currentEltCount>0){
			remove(0);
		}
	}
	
	node * moveBy(int i){
        node *t=tete;
        for(int j=0;j<i;++j){
            t=t->nextNode;
        }
        return t;
    }
	
	int count(){return currentEltCount;}
	
	float get(int index){
		if(index < 0 || index >= currentEltCount){throw Exception("Your index is out of range.");}
		
		return moveBy(index)->valeurContenue;
	}
	
	void  insert(float x, int position){
		if(position < 0 || position > currentEltCount) { throw Exception("Wrong position index.");}
		
		node * t = new node();
		t->valeurContenue = x;
		if(position == 0){
			t->nextNode = tete;
			tete = t;
		}
		else{
		node * p = moveBy(position - 1);
		t -> nextNode = p -> nextNode;
		p -> nextNode = t;
		}
		++currentEltCount;
	}
	
	void  put(float x, int position) {
		if(position < 0 || position >= currentEltCount) {throw Exception("Your position index is out of range.");}
		
		moveBy(position)->valeurContenue = x;
	}
	
	void  remove(int position) {
		if(position < 0 || position >= currentEltCount){throw Exception("Position index is not correct.");}
		node *t;
        if (position==0){
            t=tete;
            tete=tete->nextNode;
        }
        else{
            node *p=moveBy(position-1);
            t=p->nextNode;
            p->nextNode=p->nextNode->nextNode;
        }
        delete t;
        --currentEltCount;
	
	}
};

int main(){
    MyList *l=new LinkedList();
    for(int i=0;i<5;++i){
        l->insert(i,0);
        l->insert(i,l->count());
    }
    l->insert(60,5);
    for(int i=0;i<l->count();++i){
        cout<<i<<" "<<l->get(i)<<endl;
    }
    l->remove(0);
    l->remove(l->count()-1);
    l->remove(3);
    cout<<"Remove..."<<endl;
    for(int i=0;i<l->count();++i){
        cout<<i<<" "<<l->get(i)<<endl;
    }
    for(int i=0;i<l->count();++i){
        l->put(-l->get(i),i);
    }
    cout<<"put..."<<endl;
    for(int i=0;i<l->count();++i){
        cout<<i<<" "<<l->get(i)<<endl;
    }
    return 0;
}