#include <iostream>

using namespace std;


int main(){
	int a = 5;
	cout<<"un entier sans pointeur a: "<<a<<endl;
	int * pointeur1 = &a;
	cout<<"pointeur1: " <<pointeur1<<endl;
	cout<<"*pointeur1: "<<*pointeur1<<endl;
	cout<<"&pointeur1: "<<&pointeur1<<endl;
	int **doublePointeur =&pointeur1;
	cout <<"doublePointeur: "<<doublePointeur<<endl;
	cout<<"*doublePointeur: " <<*doublePointeur<<endl;
	cout<<"&doublePointeur: "<<&doublePointeur<<endl;
	cout<<"**doublePointeur: "<<**doublePointeur<<endl;
}


