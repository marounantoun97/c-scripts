#include <iostream>

using namespace std;

int factorielle(int n){
	int p = 1;
	for(int i=2; i <= n; ++i){
		p*=i;
	}
	return p;
}

int main(){
	int n = 13;
	for(int i = 0; i< n; ++i){
		cout<<"i: "<<i<<" "<<factorielle(i)<<endl;}
}



