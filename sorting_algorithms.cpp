#include <stdio.h>
#include <iostream>

using namespace std;

void triBulles(int *A, int n)
{
	cout << "Sorting array using bubble sort technique...\n";
	bool isSorted = false;
	int last = n-1;
	while (!isSorted)
	{

		isSorted = true;
		for (int j = 0; j < last; ++j)
		{
			if (A[j] > A[j + 1])
			{
				swap(A[j], A[j + 1]);
				
				isSorted = false;
			}
		}
		--last;
	}
}

void triMiniMax(int *A, int n){
	cout<<"Sorting the array using the minimax technique: "<<endl;
	bool isSorted = false;
	while (!isSorted){
		isSorted= true;
		for (int j = 0; j<n-1; ++j){
			if(A[j]>A[j+1]){
				swap(A[j], A[j+1]);
				isSorted = false;
			}
		}
		for(int j = n-1; j<0;--j){
			if(A[j]<A[j-1]){
				swap(A[j], A[j-1]);
				isSorted=false;
			}
		}
	}
}

void triSelection(int *A, int n){
	for (int i = 0; i<n; ++i){
		int minIndex = i;
		for(int j=i+1; j<n; ++j){
			if(A[j] < A[minIndex]){
				minIndex = j;
			}
		}
		if(minIndex!=i){
			swap(A[i], A[minIndex]);
		}
	}
}

void triInsertion(int*A, int n){
	for(int i = 1; i<n; ++i){
		int j=i;
		while(j>0 && A[j-1]>A[j]){
			swap(A[j], A[j-1]);
			--j;
		}
	}
}

void quickSort(int * A, int n){
	if(n>1){
	int pivot = A[0];
	int i = 1;
	int j = n-1;
	while(i <= j){
		while(A[i]<pivot && i<=j){
			++i;
		}
		while(A[j]>pivot && i<=j){
			--j;
		}
		if(i < j){
			swap(A[i], A[j]);
			++i;
			--j;
		}
	}
	A[0] = A[j];	
	A[j] = pivot;
	quickSort(A, j);
	quickSort(A+i, n-i);}
}
	int main()
{
	int n, array[50];
	cout << "Enter total number of elements :";
	cin >> n;
	cout << "Enter " << n << " numbers :";
	for (int i = 0; i < n; ++i)
	{
		cin >> array[i];
	}
	quickSort(array, n);
	cout << "Elements sorted successfully..!!\n";
	cout << "Sorted list in ascending order :\n";
	for (int i = 0; i < n; ++i)
	{
		cout << array[i] << " ";
	}
	return 0;
}