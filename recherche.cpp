#include <iostream> 

using namespace std;

bool rechercheSequentielle(int elementToBeFound, int A[], int size ){
	for(int i=0; i<size; ++i){
		if(elementToBeFound == A[i]){
			return true;
		}
	}
	return false;

}

bool rechercheDichotomie(int element, int A[], int size){
	int g = 1, d = size;
	while(d>=g){
		int m = (d + g) / 2;
		if(element == A[m]){
			return true;
		}
		if(element<A[m]){
			d = m-1;
		}
		else{
			g = m + 1;
		}
	}
	return false;
}


int main(){
	int balance[6] = {1, 2, 3, 4, 5, 6};
	bool a = rechercheSequentielle(7, balance, 6);		
	bool b = rechercheSequentielle(4, balance, 6);
	bool c = rechercheDichotomie(7, balance, 6);
	bool d = rechercheDichotomie(4, balance, 6);
	cout<<"7 par recherche sequentielle: "<<a<<endl;
	cout<<"4 par recherche sequentielle: " <<b<<endl;
	cout<<"7 par recherche par dichotomie: "<<c<<endl;
	cout<<"4 par recherche par dichotomie: "<<d<<endl;
	return 0;
}


       	
