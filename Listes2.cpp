//
//  main.cpp
//  Algo1
//
//  Created by Dany Mezher on 9/25/18.
//  Copyright © 2018 Dany Mezher. All rights reserved.
//

#include <iostream>
using namespace std;
#include <string.h>

class MyList{
  
public:
    virtual int count()=0;   // abstract int count();
    virtual float get(int i)=0; // i=0 for first element...
    virtual void insert(float x, int position)=0;
    virtual void put(float x, int position)=0;
    virtual void remove(int position)=0;
    virtual ~MyList(){}
};

class MyArrayList: public MyList{
    int n;
    int max;
    float *values;
    MyArrayList(MyArrayList &){
    }
public:
    MyArrayList(int n=10){
        this->n=0;
        this->max=n;
        values=new float[n];
    }
    
    ~MyArrayList(){
        delete[] values;
    }
    
    int count(){
        return n;
    }
    
    float get(int i){
        if (i<0 || i>=n){
            throw "Error....";
        }
        return values[i];
    }
    
    void insert(float x, int i){
        if (i<0 || i>n){
            throw "Error..";
        }
        if (n==max){
            float *tmp=values;
            values=new float[2*n];
            max=2*max;
            memcpy(values,tmp,n*sizeof(float));
//            for(int i=0;i<n;++i){
//                values[i]=tmp[i];
//            }
            delete[] tmp;
        }
        for (int j=n-1;j>=i;--j){
            values[j+1]=values[j];
        }
        values[i]=x;
        ++n;
    }
    
    void remove(int i){
        if (i<0 || i>=n){
            throw "Error...";
        }
        for(int j=i+1;j<n;++j){
            values[j-1]=values[j];
        }
        --n;
    }
    
    void put(float x, int i){
        if (i<0 || i>=n){
            throw "Error....";
        }
        values[i]=x;
    }
};

class MyLinkedList:public MyList{
    struct node{
        float v;
        node *next;
    };
    
    node *tete;
    int n;
  
public:
    MyLinkedList(){
        tete=0;
        n=0;
    }
    
    int count(){
        return n;
    }
    
    node *moveBy(int i){
        node *t=tete;
        for(int j=0;j<i;++j){
            t=t->next;
        }
        return t;
    }
    
    float get(int i){
        if (i<0 || i>=n)
            throw "....";
        return moveBy(i)->v;
    }
    void put(float x, int i){
        if (i<0 || i>=n)
            throw "....";
        moveBy(i)->v=x;
    }
    void insert(float x, int i){
        if (i<0 || i>n){
            throw ",,";
        }
        node *t=new node;
        t->v=x;
        if (i==0){
            t->next=tete;
            tete=t;
        }
        else{
            node *p=moveBy(i-1);
            t->next=p->next;
            p->next=t;
        }
        ++n;
    }
    void remove(int position){
        if (position<0 || position>=n){
            throw "...";
        }
        node *t;
        if (position==0){
            t=tete;
            tete=tete->next;
        }
        else{
            node *p=moveBy(position-1);
            t=p->next;
            p->next=p->next->next;
        }
        delete t;
        --n;
    }
    
    ~MyLinkedList(){
        while(n>0){
            remove(0);
        }
    }

};


int main(){
    MyList *l=new MyLinkedList();
    for(int i=0;i<5;++i){
        l->insert(i,0);
        l->insert(i,l->count());
    }
    l->insert(60,5);
    for(int i=0;i<l->count();++i){
        cout<<i<<" "<<l->get(i)<<endl;
    }
    l->remove(0);
    l->remove(l->count()-1);
    l->remove(3);
    cout<<"Remove..."<<endl;
    for(int i=0;i<l->count();++i){
        cout<<i<<" "<<l->get(i)<<endl;
    }
    for(int i=0;i<l->count();++i){
        l->put(-l->get(i),i);
    }
    cout<<"put..."<<endl;
    for(int i=0;i<l->count();++i){
        cout<<i<<" "<<l->get(i)<<endl;
    }
    return 0;
}
