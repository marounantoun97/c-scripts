#include <iostream>
#include <string.h>
#include <cassert>
#include <stdio.h>
#include "string"

using namespace std;
class Exception {
    std :: string _m ;
    public :
    Exception ( std :: string message ) : _m ( message ) {}
    std :: string message () const { return _m ;}
    virtual ~ Exception () {}
};

class FiFo
{
  public:
    virtual void push(float value) = 0;
    // throws Exception (" add error ") if adding fails
    virtual void pop() = 0;
    // throws Exception (" pop error ") if popping from empty
    //list
    virtual float top() = 0;
    // throws Exception (" empty list ") if popping from empty
    //list
    virtual bool isEmpty() = 0;
    virtual ~FiFo() {}
};

class ArrayFiFo : public FiFo
{
    int maxSize;
    float *values;
    int head;
    int tail;

  public:
    ArrayFiFo(int maxSize = 5)
    {
        this->maxSize = maxSize;
        values = new float[maxSize];
        this->head = -1;
        this->tail = -1;
    }
    void push(float value)
    {
        if (tail + 1 % maxSize == head)
        {

            throw "Error the queue is full...";
        }
        else if (isEmpty())
        {
            head = 0;
            tail = 0;
        }
        else
        {
            tail = (tail + 1) % maxSize;
        }

        values[tail] = value;
    }
    void pop()
    {
        if (isEmpty())
        {
            throw("pop error");
        }
        else if (head == tail)
        {
            head = -1;
            tail = -1;
        }
        else
        {
            head = (head + 1) % maxSize;
        }
    }
    float top()
    {
        if (isEmpty())
        {
            throw "Error...";
        }
        return values[head];
    }
    bool isEmpty()
    {
        if (head == -1 && tail == -1)
        {
            return true;
        }
        return false;
    }
    ~ArrayFiFo()
    {
        delete[] values;
    }
};
class LinkedFiFo : public FiFo
{
    // implement a FIFO using a linked list
  public:
    LinkedFiFo();
    void push(float value);
    void pop();
    float top();
    bool isEmpty();
    ~LinkedFiFo();
};

template <typename T>
class TEST_FIFO
{

  private:
    T *queue;

  public:
    TEST_FIFO()
    {
        queue = new T();
    }

    int startArrayTest()
    {

        queue->push(0);
        queue->push(1);
        queue->push(2);
        queue->push(3);
        queue->push(4);

        assert(queue->top() == 0);
        queue->pop();

        queue->push(5);
        try
        {
            queue->push(6);
        }
        catch (Exception e)
        {
            cout << "Array is full --> add error\n";
            cout << e.message() << endl;
        }

        assert(queue->isEmpty() == false);

        assert(queue->top() == 1);
        queue->pop();

        assert(queue->isEmpty() == false);

        assert(queue->top() == 2);
        queue->pop();
        assert(queue->top() == 3);
        queue->pop();
        assert(queue->top() == 4);
        queue->pop();

        assert(queue->isEmpty() == false);

        assert(queue->top() == 5);
        queue->pop();
        queue->push(7);
        assert(queue->top() == 7);
        queue->pop();

        assert(queue->isEmpty() == true);

        try
        {
            queue->pop();
            return 1;
        }
        catch (Exception e)
        {
            cout << "Array is empty --> pop error\n";
            cout << e.message() << endl;
        }

        return 0;
    }

    /*int startLinkedListTest()
    {

        queue->push(0);
        queue->push(1);
        queue->push(2);
        queue->push(3);
        queue->push(4);

        assert(queue->top() == 0);
        queue->pop();

        queue->push(5);
        try
        {
            queue->push(6);
        }
        catch (Exception e)
        {
            cout << e.message() << endl;
            return 1;
        }

        assert(queue->isEmpty() == false);

        assert(queue->top() == 1);
        queue->pop();
        queue->push(7);
        assert(queue->top() == 2);
        queue->pop();
        assert(queue->top() == 3);
        queue->pop();
        assert(queue->top() == 4);
        queue->pop();
        assert(queue->top() == 5);
        queue->pop();
        assert(queue->top() == 6);
        queue->pop();

        assert(queue->isEmpty() == false);

        assert(queue->top() == 7);
        queue->pop();

        assert(queue->isEmpty() == true);

        try
        {
            queue->pop();
            return 1;
        }
        catch (Exception e)
        {
            cout << "LinkedList is empty --> pop error\n";
            cout << e.message() << endl;
        }

        return 0;
    }*/
};

int main()
{

    TEST_FIFO<ArrayFiFo> testArrayFIFO = TEST_FIFO<ArrayFiFo>();
   // TEST_FIFO<LinkedFiFo> testLinkedListFIFO = TEST_FIFO<LinkedFiFo>();

    assert(testArrayFIFO.startArrayTest() == 0);
    //assert(testLinkedListFIFO.startLinkedListTest() == 0);
    return 0;
}
