#include <iostream>
using namespace std;
#include <string.h>

class Exception {
	string _m;
public :
	Exception (string message ):_m( message ){}
	string message () const { return _m ;}
	virtual ~ Exception () {}
};

class MyList{
  
public:
    virtual int count()=0;                       // abstract int count();
    virtual float get(int i)=0;                  // i=0 for first element...
    virtual void insert(float x, int position)=0;
    virtual void put(float x, int position)=0;
    virtual void remove(int position)=0;
    virtual ~MyList(){}
};


class ArrayList : public MyList{
	int nbElements;
	int capaciteMax;
	float *elements;
	ArrayList(ArrayList &){
    }
	
public:
	
	ArrayList(int nbElements = 10){
		this -> nbElements  = 0;
		this -> capaciteMax = nbElements;
		elements = new float[nbElements];
	}
	
	~ArrayList(){
		delete[] elements;
	}
	
	int count(){return nbElements;}
	
	float get(int index){
		if(index < 0 || index >= nbElements){
			throw Exception("The index you provided is out of range.");} 
		return elements[index];
	}
	
	
	void insert(float x, int position){
		if (position < 0 || position>capaciteMax){throw Exception("Error adding this element at the given position .");}
		if(nbElements == capaciteMax){
			float *tmp = elements;
			elements = new float[2*nbElements];
			capaciteMax = 2 * capaciteMax;
			for(int i = 0; i<position; ++i){
				elements[i] = tmp[i];
			}
			elements[position] = x;
			for(int i = position; i<nbElements+1; ++i){
			elements[i+1] = tmp[i];}
			
			delete[] tmp;	
		}
		else{
			for (int j=nbElements-1;j>=position;--j){
            elements[j+1]=elements[j];
			}
			
			elements[position]=x;
		}
		++nbElements;
	}
	
	void put(float x, int position){
		if(position < 0 || position > nbElements){
			throw Exception("The position you provided is out of range.");}
		elements[position] = x;
	}
	
	void remove(int position){
		if (position < 0 || position>capaciteMax){throw Exception("Error removing the element at this given position .");}
		for (int i = position; i<nbElements; ++i){
			elements[i] = elements[i+1];
		}
		--nbElements;
	}
	
	
	
};

int main(){
    MyList *l=new ArrayList(5);
    for(int i=0;i<5;++i){
        l->insert(i,0);
        l->insert(i,l->count());
    }
    l->insert(60,5);
    for(int i=0;i<l->count();++i){
        cout<<i<<" "<<l->get(i)<<endl;
    }
    l->remove(0);
    l->remove(l->count()-1);
    l->remove(3);
    cout<<"Remove..."<<endl;
    for(int i=0;i<l->count();++i){
        cout<<i<<" "<<l->get(i)<<endl;
    }
    for(int i=0;i<l->count();++i){
        l->put(-l->get(i),i);
    }
    cout<<"put..."<<endl;
    for(int i=0;i<l->count();++i){
        cout<<i<<" "<<l->get(i)<<endl;
    }
    return 0;
}
