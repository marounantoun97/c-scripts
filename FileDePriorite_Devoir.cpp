// Maroun Antoun 160180

#include <iostream>
#include <string>
#include <stdio.h>
#include <cassert>

using namespace std;

class Exception
{
    std ::string _m;

  public:
    Exception(std ::string message) : _m(message) {}
    std ::string message() const { return _m; }
    virtual ~Exception() {}
};

class PriorityQueue
{
  private:
    int capacity;
    int heap_size;
    float *heap;

  public:
    PriorityQueue(int cap)
    {
        this->heap_size = 0;
        this->capacity = cap;
        heap = new float[cap];
    }
    int parent(int i)
    {
        return (i - 1) / 2;
    }
    int filsGauche(int i)
    {
        return (2 * i + 1);
    }
    int filsDroit(int i)
    {
        return (2 * i + 2);
    }

    void push(float value)
    {
        if (heap_size == capacity)
        {
            throw Exception(" add error");
        }

        int i = heap_size;
        heap[i] = value;
        ++heap_size;
        while (i != 0 && heap[parent(i)] < heap[i])
        {
            swap(heap[i], heap[parent(i)]);
            i = parent(i);
        }
    }

    void pop()
    {
        if (heap_size == 0)
        {
            throw Exception(" empty ");
        }
        if (heap_size == 1)
        {
            --heap_size;
        }

        if (heap_size > 1)
        {
            heap[0] = heap[heap_size - 1];
            --heap_size;
            for (int i = 0; i < heap_size - 1; ++i)
            {
                if (hasChildren(i))
                {
                    float maximum = max(heap[filsDroit(i)], heap[filsGauche(i)]);
                    if (heap[i] < maximum)
                    {
                        int maxIndex;
                        if (maximum == heap[filsDroit(i)])
                        {
                            maxIndex = filsDroit(i);
                        }
                        else
                        {
                            maxIndex = filsGauche(i);
                        }

                        swap(heap[i], heap[maxIndex]);
                    }
                }
            }
        }
    }

    float top()
    {
        if (heap_size == 0)
        {
            throw Exception(" empty ");
        }

        return heap[0];
    }

    bool hasChildren(int i)
    {

        if (((2 * i) + 1) >= heap_size)
        {
            return false;
        }

        else
        {
            return true;
        }
    }

    ~PriorityQueue()
    {
        delete[] heap;
    }

    // Following methods are used to validate your code , should
    // not be included in a regular PriorityQueue
    int size()
    {
        return heap_size;
    }

    float get(int i)
    {
        if (i < 0 || i > heap_size)
        {
            throw Exception("IndexOutOfBound");
        }
        return heap[i];
    }
};


